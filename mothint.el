(require 'thingatpt)

(defun thing-at-point--end-of-integer ()
  "Go to the end of the integer at point."
  (let ((inhibit-changing-match-data t))
    (when (looking-at "[+-]")
      (forward-char 1))
    (skip-chars-forward "[:digit:]")
    (unless (looking-back "[[:digit:]]" nil)
      (error "No integer here"))))

(defun thing-at-point--beginning-of-integer ()
  "Go to the beginning of the integer at point."
  (let ((inhibit-changing-match-data t))
    (skip-chars-backward "[:digit:]")
    (unless (looking-at "[+-]?[[:digit:]]")
      (error "No integer here"))
    (when (looking-back "[+-]" nil)
      (backward-char 1))))

(defun thing-at-point--bounds-of-integer-at-point ()
  "Get boundaries of integer at point."
  (save-excursion
    (let (beg end)
      (thing-at-point--beginning-of-integer)
      (setq beg (point))
      (thing-at-point--end-of-integer)
      (setq end (point))
      (cons beg end))))

(defun thing-at-point-integer-at-point ()
  "Get integer at point."
  (let ((bounds (bounds-of-thing-at-point 'integer)))
    (string-to-number (buffer-substring (car bounds) (cdr bounds)))))

(put 'integer 'beginning-op 'thing-at-point--beginning-of-integer)
(put 'integer 'end-op 'thing-at-point--end-of-integer)
(put 'integer 'bounds-of-thing-at-point 'thing-at-point--bounds-of-integer-at-point)
(put 'integer 'thing-at-point 'thing-at-point-integer-at-point)


(defun mothint--operate-number-at-point (operation &optional arg)
  (if (not (numberp (thing-at-point 'integer)))
      (error "thing at point is not a number"))
  (let ((start-pos (point))
        (line-number-at-start (line-number-at-pos))
        (operand (or arg 1))
        (value (thing-at-point 'integer)))
    (save-excursion
      (let ((bounds (bounds-of-thing-at-point 'integer)))
        (kill-region (car bounds) (cdr bounds))
        (insert (number-to-string (funcall operation value operand)))
        (pop kill-ring)))
    (goto-char start-pos)
    (if (> (line-number-at-pos) line-number-at-start)
      (forward-char -1))))

(defun mothint-multiply-number-at-point (&optional arg)
  (interactive "p")
  (mothint--operate-number-at-point '* arg))

(defun mothint-divide-number-at-point (&optional arg)
  (interactive "p")
  (mothint--operate-number-at-point '/ arg))

(defun mothint-increment-number-at-point (&optional arg)
  (interactive "p")
  (mothint--operate-number-at-point '+ arg))

(defun mothint-decrement-number-at-point (&optional arg)
  (interactive "p")
  (let ((increment (or arg 1)))
    (mothint-increment-number-at-point (- 0 increment))))

(provide 'mothint)
